package testSuites.scenarios;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import stepdefinitions.workFlow.LoginPageStep;
import stepdefinitions.workFlow.RequestTrackingStep;
import stepdefinitions.workFlow.TimeMonitoringStep;

public class NewClientScenarios {

    @Steps
    LoginPageStep loginWF;

    @Steps
    stepdefinitions.workFlow.HomePageStep homePageWF;

    @Steps
    RequestTrackingStep request;

    @Steps
    TimeMonitoringStep monitoring;

    @When("Login into WorkFlow")
    public void loginWorkFlow(){
        loginWF.navigateWF();
        loginWF.setCredentials();
    }

    @Then("Select {string} country in WorkFlow")
    public void countryWorkFlow(String country){
        homePageWF.selectCountry(country);
        homePageWF.selectLanguage();
    }

    @Then("Select {string} menu in WorkFlow")
    public void menuWorkFlow(String menu){
        homePageWF.selectMenu(menu);
    }

    @Then("Select {string} menu and assign request in WorkFlow {string}")
    public void assignRequest(String menu, String document){
        homePageWF.selectMenu(menu);
        monitoring.allin(document);
    }

    @Then("Approved client request with document {string}")
    public void searchRequest(String document){
        request.searchByTRN(document);
        request.dataValidationClient();
        request.confirmValidationClient();
        request.approvedClient();
    }

}
