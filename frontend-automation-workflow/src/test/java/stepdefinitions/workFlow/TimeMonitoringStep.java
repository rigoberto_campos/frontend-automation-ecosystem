package stepdefinitions.workFlow;

import net.thucydides.core.annotations.Step;
import pageStructures.workFlow.TimeMonitoring;

public class TimeMonitoringStep {
    TimeMonitoring tm;

    @Step("All in")
    public void allin(String trn){
        tm.searchByDocument();
        tm.selectDocument();
        tm.enterDocument(trn);
        tm.clickSearch();
        tm.selectRequest();
        tm.assignRequest();
        tm.selectUser();
        tm.confirmBtn();
    }
}
