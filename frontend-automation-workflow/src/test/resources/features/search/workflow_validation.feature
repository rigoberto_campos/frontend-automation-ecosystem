@WorkFlow
Feature: WorkFlow validation

  @signup
  @assign
  Scenario: Request tracking client in WorkFlow
        When Login into WorkFlow
        Then Select "Jamaica" country in WorkFlow
        Then Select "Time Monitoring" menu and assign request in WorkFlow "976345108"
        Then Select "Request Tracking" menu in WorkFlow
        Then Approved client request with document "976345108"

  @signup
  @validation
  Scenario: Request tracking client in WorkFlow
        When Login into WorkFlow
        Then Select "Jamaica" country in WorkFlow
        Then Select "Request Tracking" menu in WorkFlow
        Then Approved client request with document "976345108"
