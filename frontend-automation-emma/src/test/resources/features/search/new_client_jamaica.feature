@Emma
Feature: New client request Emma

    @newClient
    @signup
    Scenario: New credit application Jamaica
        When Login into Emma
        Then Select "Jamaica" country
        Then Create new client
        Then Complete client personal information when client is pp "Yes"
        Then Complete client region information from "Jamaica" "Hanover" "Ramble"
        Then Search client status from "Jamaica"

    @signup
    @cash
    Scenario: Create cash loan Emma
        When Login into Emma
        Then Select "Jamaica" country
        Then Verify "179492683" client is approved
        Then Loan quoter with client TRN

    @signup
    @passport
    @Aruba
    Scenario: New credit application Aruba Emma
        When Login into Emma
        Then Select "Aruba" country
        Then Create new client
        Then Complete client personal information
        Then Complete client region information from "Aruba" "Savaneta" "Cura Cabai"
        Then Search client status from "Aruba"
