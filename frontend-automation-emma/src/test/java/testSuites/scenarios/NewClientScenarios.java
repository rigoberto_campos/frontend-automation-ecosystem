package testSuites.scenarios;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.IdCalculator;
import stepdefinitions.emma.HomePageStep;
import stepdefinitions.emma.LoanQuoterStep;
import stepdefinitions.emma.NewClientPageStep;

public class NewClientScenarios {
    @Steps
    stepdefinitions.emma.LoginPageStep loginPage;

    @Steps
    HomePageStep homePage;

    @Steps
    NewClientPageStep newClientPage;

    @Steps
    LoanQuoterStep loan;

    @When("Login into Emma")
    public void loginEmma(){
        loginPage.openEmma();
        loginPage.loginEmma();
        loginPage.confirmEmma();
    }

    @Then("Select {string} country")
    public void selectCountry(String country){
        homePage.selectCountry(country);
    }

    @Then("Create new client")
    public void newClient(){
        homePage.createNewClient();
    }

    @Then("Complete client personal information when client is pp {string}")
    public void newClientInfo(String political){
        newClientPage.insertTRN();
        newClientPage.personalInfo();
        newClientPage.personalInfoPayment();
        newClientPage.personalWorkInfo(political);
    }

    @Then("Complete client region information from {string} {string} {string}")
    public void regionInformation(String country, String region1, String region2){
        newClientPage.geographicInfo(region1, region2);
        newClientPage.contactInfo(country);
        newClientPage.filesInfo();
    }

    @Then("Verify {string} client is approved")
    public void verifyTrn(String document){
        IdCalculator.getInstance().setTRN(document);
        homePage.verifyTrn();
    }

    @Then("Search client status from {string}")
    public void clientStatus(String country){
        homePage.searchClientStatus(country);
    }

    @Then("Loan quoter with client TRN")
    public void loanProcess(){
        homePage.clickLoan();
        loan.validateLoan();
        loan.calculateQuote();
    }

}
