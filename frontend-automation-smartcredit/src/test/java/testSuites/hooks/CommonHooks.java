package testSuites.hooks;

import io.cucumber.java.Before;
import starter.ConstantReader;
import starter.IdCalculator;

public class CommonHooks {
    @Before("@signup")
    public void setUp(){
        ConstantReader.getInstance().readFile();
        IdCalculator.getInstance();
    }

    @Before("@passport")
    public void setUpPassport(){
        IdCalculator.getInstance().setPassport();
    }
}
