package testSuites.scenarios;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import stepdefinitions.smartCredit.LoginPageStep;

public class NewClientScenarios {

    @Steps
    LoginPageStep lps;

    @When("Navigate to Smart Credit")
    public void navigate(){
        lps.navigate();
    }

}
