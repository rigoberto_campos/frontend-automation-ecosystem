package pageStructures.smartcredit;

import constants.SystemConstants;
import helpers.PageAction;
import starter.ConstantReader;

public class LoginPage extends PageAction {


    public void navigateToSmartCredit(){
        navigateTo(ConstantReader.getInstance().getProperties().get(SystemConstants.SMARTCREDIT_URL));
    }
}
