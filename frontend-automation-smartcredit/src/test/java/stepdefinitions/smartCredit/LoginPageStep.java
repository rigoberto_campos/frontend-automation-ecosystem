package stepdefinitions.smartCredit;

import net.thucydides.core.annotations.Step;
import pageStructures.smartcredit.LoginPage;

public class LoginPageStep {

    private LoginPage lp;

    @Step("Navigate to Smart credit")
    public void navigate(){
        lp.navigateToSmartCredit();
    }
}
